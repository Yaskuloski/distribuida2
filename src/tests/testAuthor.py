from entities import Author
from . import TestSuite
from app import db
import json

class TestAuthor(TestSuite):
    def testGetAuthor(self):
        with self.app.app_context():
            # Cargo autores en la db
            data = {'name':'Nicolas','surname':'Yaskuloski'}
            author = Author(data)
            db.session.add(author)
            db.session.commit()

        """def test_redirect_to_login(self):
            res = self.client.get('/admin/')
            self.assertEqual(302, res.status_code)
            self.assertIn('login', res.location)"""

        # Traigo los datos
        res = self.client.get('author/1')
        self.assertEqual(res.status_code, 200)
        #print(res)
        #pepe = json.loads(res.data.decode())
        res = json.loads(res.data.decode())
        #print(res)

        # Valido que sean iguales a los que envie
        self.assertEqual(data['name'], res.get('name'))
        self.assertEqual(data['surname'], res.get('surname'))


    def testGetAuthors(self):
        with self.app.app_context():
            # Cargo autores en la db
            data1 = {'name':'Nicolas','surname':'Yaskuloski'}
            author1 = Author(data1)
            db.session.add(author1)

            data2 = {'name':'Nicolas','surname':'Yaskuloski'}
            author2 = Author(data2)
            db.session.add(author2)

            db.session.commit()

        res = self.client.get('authors')
        self.assertEqual(res.status_code, 200)
        res = json.loads(res.data.decode())
        #print (res)

        self.assertEqual(len(res), 2)

        self.assertEqual(data1['name'], res[0].get('name'))
        self.assertEqual(data1['surname'], res[0].get('surname'))

        self.assertEqual(data2['name'], res[1].get('name'))
        self.assertEqual(data2['surname'], res[1].get('surname'))

    def testAuthorError(self):
        res = self.client.get('author/1')
        self.assertEqual(res.status_code,404)

        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testAuthorsError(self):
        res = self.client.get('authors')
        self.assertEqual(res.status_code,404)

        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entities'))