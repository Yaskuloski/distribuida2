import re
from entities import Partner
from . import TestSuite
from app import db
import json

partner1 = {
    'nickname' : 'Nico',
    'mail' : 'nico@yaskuloski.net',
}
partner2 = {
    'nickname' : 'Pepe',
    'mail' : 'pepe@partner.dos'
}
partnerError = {}

# Datos a completar en la entidad
# nickname, mail
class TestPartner(TestSuite):
    def testCreatePartner(self):
        res = self.client.post('/partner',data=json.dumps(partner1),content_type='application/json')
        self.assertEqual(res.status_code, 201)

        # Valido la respuesta
        res = json.loads(res.data.decode())
        self.assertEqual(1, res.get('id'))
        self.assertEqual(partner1['nickname'], res.get('nickname'))
        self.assertEqual('OK', res.get('creation'))

        # Valido el contenido de la base de datos
        dbQuery = self.client.get('partner/1')
        dbQuery = json.loads(dbQuery.data.decode())
        self.assertEqual(1, dbQuery.get('id'))
        self.assertEqual(partner1['nickname'], dbQuery.get('nickname'))
        self.assertEqual(partner1['mail'], dbQuery.get('mail'))

    def testGetPartner(self):
        with self.app.app_context():
            partner = Partner(partner1)
            db.session.add(partner)
            db.session.commit()

        # Valido la respuesta
        res = self.client.get('partner/1')
        self.assertEqual(res.status_code, 200)

        res = json.loads(res.data.decode())
        self.assertEqual(1, res.get('id'))
        self.assertEqual(partner1['nickname'], res.get('nickname'))
        self.assertEqual(partner1['mail'], res.get('mail'))

    def testGetPartners(self):
        with self.app.app_context():
            data1 = Partner(partner1)
            db.session.add(data1)

            data2 = Partner(partner2)
            db.session.add(data2)

            db.session.commit()

        # Valido la respuesta
        res = self.client.get('partners')
        self.assertEqual(res.status_code, 200)
        res = json.loads(res.data.decode())

        # Partner 1
        self.assertEqual(1, res[0].get('id'))
        self.assertEqual(partner1['nickname'], res[0].get('nickname'))
        self.assertEqual(partner1['mail'], res[0].get('mail'))
        # Partner 2
        self.assertEqual(2, res[1].get('id'))
        self.assertEqual(partner2['nickname'], res[1].get('nickname'))
        self.assertEqual(partner2['mail'], res[1].get('mail'))

    def testUpdatePartner(self):
        with self.app.app_context():
            partner = Partner(partner1)
            db.session.add(partner)
            db.session.commit()

        updatedPartner = {
            'nickname' : 'Modificado',
            'mail' : 'nico@modificado'
        }

        # Valido la respuesta
        res = self.client.put('partner/1',data=json.dumps(updatedPartner),content_type='application/json')
        res = json.loads(res.data.decode())
        self.assertEqual(1, res.get('id'))
        self.assertEqual('Partner updated OK', res.get('update'))

        # Valido la base de datos
        dbQuery = self.client.get('partner/1')
        dbQuery = json.loads(dbQuery.data.decode())
        self.assertEqual(1, res.get('id'))
        self.assertEqual(updatedPartner['nickname'], dbQuery.get('nickname'))
        self.assertEqual(updatedPartner['mail'], dbQuery.get('mail'))

        # Valido con el partner creado en principio la diferencia con el nuevo
        self.assertEqual(1, res.get('id'))
        self.assertNotEqual(partner1['nickname'], dbQuery.get('nickname'))
        self.assertNotEqual(partner1['mail'], dbQuery.get('mail'))

    def testDeletePartner(self):
        with self.app.app_context():
            partner = Partner(partner1)
            db.session.add(partner)
            db.session.commit()

        # Valido la respuesta
        res = self.client.delete('partner/1')
        res = json.loads(res.data.decode())
        self.assertEqual('Partner deleted ok', res.get('delete'))
        self.assertEqual(partner1['nickname'], res.get('nickname'))

        # Valido el estado de la base de datos
        dbQuery = self.client.get('partners')
        self.assertEqual(dbQuery.status_code, 404)
        dbQuery = json.loads(dbQuery.data.decode())
        self.assertEqual('empty', dbQuery.get('Entities'))


    def testCreatePartnerError(self):
        # Valido error al crear un partner
        res = self.client.post('partner',data=json.dumps(partnerError),content_type='application/json')
        self.assertEqual(res.status_code, 201)
        #res = self.client.post('partner',data=json.dumps(partnerError),content_type='application/json')
        #self.assertEqual(res.status_code, 404)

    def testGetPartnerError(self):
        # Valido error al traer partner no existente
        res = self.client.get('partner/0')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testGetPartnersError(self):
        # Valido error al traer todos los partners con la db vacia
        res = self.client.get('partners')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entities'))

    def testUpdateBooksError(self):
        # Valido error al actualizar partner no existente
        res = self.client.put('book/1',data=json.dumps(partnerError),content_type='application/json')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testDeleteBookError(self):
        # Valido error al eliminar partner no existente
        res = self.client.delete('partner/1')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))