from entities import Editorial
from . import TestSuite
from app import db
import json

class TestEditorial(TestSuite):
    def testGetEditorial(self):
        with self.app.app_context():
            data = {'name':'arcoiris'}
            editorial = Editorial(data)
            db.session.add(editorial)
            db.session.commit()

        res = self.client.get('editorial/1')
        self.assertEqual(res.status_code, 200)

        res = json.loads(res.data.decode())
        self.assertEqual(data['name'], res.get('name'))


    def testGetEditorials(self):
        with self.app.app_context():
            data1 = {'name':'dunken'}
            editorial1 = Editorial(data1)
            db.session.add(editorial1)

            data2 = {'name':'imperial'}
            editorial2 = Editorial(data2)
            db.session.add(editorial2)

            db.session.commit()

        res = self.client.get('editorials')
        self.assertEqual(res.status_code, 200)
        res = json.loads(res.data.decode())

        self.assertEqual(len(res), 2)

        self.assertEqual(data1['name'], res[0].get('name'))
        self.assertEqual(data2['name'], res[1].get('name'))

    def testEditorialError(self):
        res = self.client.get('editorial/1')
        self.assertEqual(res.status_code,404)

        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testEditorialsError(self):
        res = self.client.get('editorials')
        self.assertEqual(res.status_code,404)

        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entities'))