from flask import Flask
from flask_sqlalchemy import SQLAlchemy
#from entities import *

# app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root@localhost/flaskmysql-test'
# app.config['TESTING']=True
# app.config['DEBUG']=True
# # Para evitar los warning en consola
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False

# db = SQLAlchemy(app)
# db.init_app(app)

def createApp():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root@localhost/flaskmysql-test'
    app.config['TESTING']=True
    app.config['DEBUG']=True
    # Para evitar los warning en consola
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
    db.init_app(app)
    return app

db = SQLAlchemy()
