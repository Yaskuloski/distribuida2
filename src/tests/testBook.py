from entities import Book, Author, Genre, Editorial
from . import TestSuite
from app import db
import json

book1 = {
    'title' : 'El Barco',
    'isbn' : '10289301283-901283912-2323',
    'authorId' : 1,
    'genreId' : 1,
    'editorialId' : 1,
}

book2 = {
    'title' : 'Asedio',
    'isbn' : '10289301283-901283912-2323-3',
    'authorId' : 2,
    'genreId' : 2,
    'editorialId' : 2,
}

bookError = {
    'title' : 'Recientes',
    'isbn' : 'isbn Error',
    'authorId' : 6,
    'genreId' : 2,
    'editorialId' : 2,
}

# Datos a completar en la entidad
# title, isbn, authorId, genreId, editorialId
class TestBook(TestSuite):
    def setUp(self):
        super().setUp()
        with self.app.app_context():
            # Cargo autores en la db para uso futuro
            data1 = {'name':'Nicolas','surname':'Yaskuloski'}
            author1 = Author(data1)
            db.session.add(author1)

            data2 = {'name':'Nicolas','surname':'Yaskuloski'}
            author2 = Author(data2)
            db.session.add(author2)

            # Cargo generos en la db para uso futuro
            data1 = {'name':'aventura'}
            genre1 = Genre(data1)
            db.session.add(genre1)

            data2 = {'name':'terror'}
            genre2 = Genre(data2)
            db.session.add(genre2)

            # Cargo editoriales en la db para uso futuro
            data1 = {'name':'dunken'}
            editorial1 = Editorial(data1)
            db.session.add(editorial1)

            data2 = {'name':'imperial'}
            editorial2 = Editorial(data2)
            db.session.add(editorial2)

            db.session.commit()

    def testCreateBook(self):
        res = self.client.post('/book',data=json.dumps(book1),content_type='application/json')
        self.assertEqual(res.status_code, 201)

        res = json.loads(res.data.decode())
        self.assertEqual(book1['title'], res.get('title'))
        self.assertEqual('OK', res.get('creation'))

        # Valido el contenido de la base de datos
        dbQuery = self.client.get('book/1')
        dbQuery = json.loads(dbQuery.data.decode())
        self.assertEqual(book1['title'], dbQuery.get('title'))
        self.assertEqual(book1['isbn'], dbQuery.get('isbn'))
        self.assertEqual(book1['authorId'], dbQuery.get('authorId'))
        self.assertEqual(book1['genreId'], dbQuery.get('genreId'))
        self.assertEqual(book1['editorialId'], dbQuery.get('editorialId'))

    def testGetBook(self):
        with self.app.app_context():
            book = Book(book1)
            db.session.add(book)
            db.session.commit()

        res = self.client.get('book/1')
        self.assertEqual(res.status_code, 200)

        res = json.loads(res.data.decode())
        self.assertEqual(book1['title'], res.get('title'))
        self.assertEqual(book1['isbn'], res.get('isbn'))
        self.assertEqual(book1['authorId'], res.get('authorId'))
        self.assertEqual(book1['genreId'], res.get('genreId'))
        self.assertEqual(book1['editorialId'], res.get('editorialId'))

    def testGetBooks(self):
        with self.app.app_context():
            book1test = Book(book1)
            db.session.add(book1test)

            book2test = Book(book2)
            db.session.add(book2test)

            db.session.commit()

        res = self.client.get('books')
        self.assertEqual(res.status_code, 200)

        res = json.loads(res.data.decode())
        # Primer libro
        self.assertEqual(book1['title'], res[0].get('title'))
        self.assertEqual(book1['isbn'], res[0].get('isbn'))
        self.assertEqual(book1['authorId'], res[0].get('authorId'))
        self.assertEqual(book1['genreId'], res[0].get('genreId'))
        self.assertEqual(book1['editorialId'], res[0].get('editorialId'))
        # Segundo libro
        self.assertEqual(book2['title'], res[1].get('title'))
        self.assertEqual(book2['isbn'], res[1].get('isbn'))
        self.assertEqual(book2['authorId'], res[1].get('authorId'))
        self.assertEqual(book2['genreId'], res[1].get('genreId'))
        self.assertEqual(book2['editorialId'], res[1].get('editorialId'))

    def testUpdateBook(self):
        with self.app.app_context():
            book = Book(book1)
            db.session.add(book)
            db.session.commit()

        updatedBook = {
            'title' : 'El Barco 2.0',
            'isbn' : '10289301283-901283912-2323-2.0',
            'authorId' : 2,
            'genreId' : 2,
            'editorialId' : 2,
        }
        
        # Valido la respuesta
        res = self.client.put('book/1',data=json.dumps(updatedBook),content_type='application/json')
        res = json.loads(res.data.decode())
        self.assertEqual(1, res.get('id'))
        self.assertEqual('Book updated OK', res.get('update'))

        # Valido la base de datos
        dbQuery = self.client.get('book/1')
        dbQuery = json.loads(dbQuery.data.decode())
        self.assertEqual(updatedBook['title'], dbQuery.get('title'))
        self.assertEqual(updatedBook['isbn'], dbQuery.get('isbn'))
        self.assertEqual(updatedBook['authorId'], dbQuery.get('authorId'))
        self.assertEqual(updatedBook['genreId'], dbQuery.get('genreId'))
        self.assertEqual(updatedBook['editorialId'], dbQuery.get('editorialId'))

        # Valido con el libro creado en principio la diferencia con el nuevo
        self.assertNotEqual(book1['title'], dbQuery.get('title'))
        self.assertNotEqual(book1['isbn'], dbQuery.get('isbn'))
        self.assertNotEqual(book1['authorId'], dbQuery.get('authorId'))
        self.assertNotEqual(book1['genreId'], dbQuery.get('genreId'))
        self.assertNotEqual(book1['editorialId'], dbQuery.get('editorialId'))

    def testDeleteBook(self):
        with self.app.app_context():
            book = Book(book1)
            db.session.add(book)
            db.session.commit()

        # Valido la respuesta
        res = self.client.delete('book/1')
        res = json.loads(res.data.decode())
        self.assertEqual('Book deleted ok', res.get('delete'))
        self.assertEqual(book1['title'], res.get('title'))

        # Valido el estado de la base de datos
        dbQuery = self.client.get('books')
        self.assertEqual(dbQuery.status_code, 404)
        dbQuery = json.loads(dbQuery.data.decode())
        self.assertEqual('empty', dbQuery.get('Entities'))

    def testCreateBookError(self):
        # Valido error al crear un libro
        res = self.client.post('/book',data=json.dumps(bookError),content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def testGetBookError(self):
        # Valido error al traer libro no existente
        res = self.client.get('book/0')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testGetBooksError(self):
        # Valido error al traer todos los libros con la db vacia
        res = self.client.get('books')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entities'))

    def testUpdateBooksError(self):
        # Valido error al actualizar libro no existente
        updatedBook = {
            'title' : 'El Barco 2.0',
            'isbn' : '10289301283-901283912-2323-2.0',
            'authorId' : 2,
            'genreId' : 2,
            'editorialId' : 2,
        }
        
        # Valido la respuesta
        res = self.client.put('book/1',data=json.dumps(updatedBook),content_type='application/json')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testDeleteBookError(self):
        # Valido error al eliminar libro no existente
        res = self.client.delete('book/1')
        self.assertEqual(res.status_code, 404)
        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))