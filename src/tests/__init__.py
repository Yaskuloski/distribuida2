import unittest
from app import createApp, db

class TestSuite(unittest.TestCase):

    # Tengo que cargar un entorno para test para no estar trabajando sobre la db principal
    def setUp(self):
        self.app = createApp('ConfigEnvironmentTest')
        self.client = self.app.test_client()
        
        with self.app.app_context():
            db.create_all()
        

    # #Acá es donde se ejecutarían los test de cada path de la api
    # #Haciendo que herede de TestSuite
    # # #def test_mi_test(self):
    #     # Código que se quiere probar
        

    # Tengo que borrar todas las tablas de la db test para arrancar de cero en cada test
    def tearDown(self):
        with self.app.app_context():
            # Elimina todas las tablas de la base de datos
            db.session.remove()
            db.drop_all()