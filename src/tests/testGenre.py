from entities import Genre
from . import TestSuite
from app import db
import json

class TestGenre(TestSuite):
    def testGetGenre(self):
        with self.app.app_context():
            data = {'name':'Accion'}
            genre = Genre(data)
            db.session.add(genre)
            db.session.commit()

        res = self.client.get('genre/1')
        self.assertEqual(res.status_code, 200)

        res = json.loads(res.data.decode())
        self.assertEqual(data['name'], res.get('name'))


    def testGetGenres(self):
        with self.app.app_context():
            data1 = {'name':'aventura'}
            genre1 = Genre(data1)
            db.session.add(genre1)

            data2 = {'name':'terror'}
            genre2 = Genre(data2)
            db.session.add(genre2)

            db.session.commit()

        res = self.client.get('genres')
        self.assertEqual(res.status_code, 200)
        res = json.loads(res.data.decode())

        self.assertEqual(len(res), 2)

        self.assertEqual(data1['name'], res[0].get('name'))
        self.assertEqual(data2['name'], res[1].get('name'))

    def testGenreError(self):
        res = self.client.get('genre/1')
        self.assertEqual(res.status_code,404)

        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entity'))

    def testGenresError(self):
        res = self.client.get('genres')
        self.assertEqual(res.status_code,404)

        res = json.loads(res.data.decode())
        self.assertEqual('empty', res.get('Entities'))