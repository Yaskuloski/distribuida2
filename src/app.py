#from serverDB import app, db
# from services import *
# from entities import *
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
#from services import *

# if __name__ == '__main__':
#     db.create_all()
#     # Se setea debug en true para poder realizar pruebas una vez terminado
#     # el desarrollo y al tener que realizar alguna correccion en el codigo 
#     # al guardar se vuelva a lanzar la app
#     app.run(debug=True)

db = SQLAlchemy()

#configuro el parametro por defecto 
def createApp(config='ConfigEnvironment'):
    # import services
    from services import paths
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('environment.{}'.format(config))
    app.register_blueprint(paths)
    db.init_app(app)
    return app

#No andaba nada por culpa de esto

# app = createApp()
# if __name__ == '__main__':
#     with app.app_context():
#         db.create_all()
#     app.run(debug=True)