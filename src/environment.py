# from re import DEBUG
# from flask import Flask
# from flask_sqlalchemy import SQLAlchemy

# app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root@localhost/flaskmysql'
# # Para evitar los warning en consola
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False

# db = SQLAlchemy(app)

class ConfigEnvironment():
    SQLALCHEMY_DATABASE_URI='mysql+pymysql://root@localhost/flaskmysql'
    SQLALCHEMY_TRACK_MODIFICATIONS=False

class ConfigEnvironmentTest():
    from os import environ
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI_TEST") if environ.get("SQLALCHEMY_DATABASE_URI_TEST") else 'mysql+pymysql://root@localhost/flaskmysql-test'

    # SQLALCHEMY_DATABASE_URI='mysql+pymysql://root@localhost/flaskmysql-test'
    # SQLALCHEMY_DATABASE_URI='mysql+pymysql://root:1234test@mysql/flaskmysql-test'
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    TESTING = True
    DEBUG = True
