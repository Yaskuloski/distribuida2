#from serverDB import app, db
from entities import * 
from flask import jsonify, request, Blueprint
#from app import db

paths = Blueprint('path',__name__)

@paths.route('/', methods=['GET'])
def default():
    return jsonify({
        'TP':'Distribuida 1',
        'UNDAV':'Universidad Nacional de Avellaneda',
        'Alumno':'Yaskuloski Nicolas',
        'Profesor':'Martin Miguel Machuca'
    })

@paths.route('/author/<int:id>', methods=['GET'])
def getAuthor(id):
    author = Author.query.get(id)

    if author is None: return jsonify({'Entity':'empty'}),404

    return jsonify({
        'id':author.id,
        'name':author.name,
        'surname':author.surname
    })

@paths.route('/authors', methods=['GET'])
def getAuthors():
    authors = Author.query.order_by(Author.id).all()

    if not authors : return jsonify({'Entities':'empty'}),404

    return jsonify([{
        'id':author.id,
        'name':author.name,
        'surname':author.surname
    }for author in authors])

@paths.route('/genre/<int:id>', methods=['GET'])
def getGenre(id):
    genre = Genre.query.get(id)

    if genre is None: return jsonify({'Entity':'empty'}),404

    return jsonify({
        'id':genre.id,
        'name':genre.name
    })

@paths.route('/genres', methods=['GET'])
def getGenres():
    genres = Genre.query.order_by(Genre.id).all()

    if not genres: return jsonify({'Entities':'empty'}),404

    return jsonify([{
        'id':genre.id,
        'name':genre.name
    }for genre in genres])

@paths.route('/editorial/<int:id>', methods=['GET'])
def getEditorial(id):
    editorial = Editorial.query.get(id)

    if editorial is None: return jsonify({'Entity':'empty'}),404

    return jsonify({
        'id':editorial.id,
        'name':editorial.name
    })

@paths.route('/editorials', methods=['GET'])
def getEditorials():
    editorials = Editorial.query.order_by(Editorial.id).all()

    if not editorials: return jsonify({'Entities':'empty'}),404

    return jsonify([{
        'id':editorial.id,
        'name':editorial.name
    }for editorial in editorials])

#ENTIDADES CON CRUD

@paths.route('/book', methods=['POST'])
def createBook():
    
    try:
        newBook = Book(request.get_json())

        db.session.add(newBook)
        db.session.commit()
        
        return jsonify({
            'id':newBook.id,
            'title':newBook.title,
            'creation':'OK'
            }),201
    
    except Exception as e:
        return jsonify({'ERROR':str(e)}),404

@paths.route('/book/<int:id>', methods=['GET'])
def getBook(id):
    book = Book.query.get(id)

    if book is None: return jsonify({'Entity':'empty'}),404

    return jsonify({
        'id':book.id,
        'title':book.title,
        'isbn':book.isbn,
        'authorId':book.authorId,
        'genreId':book.genreId,
        'editorialId':book.editorialId
    })

@paths.route('/books', methods=['GET'])
def getBooks():
    books = Book.query.order_by(Book.id).all()

    if not books: return jsonify({'Entities':'empty'}),404

    return jsonify([{
        'id':book.id,
        'title':book.title,
        'isbn':book.isbn,
        'authorId':book.authorId,
        'genreId':book.genreId,
        'editorialId':book.editorialId
    }for book in books])

@paths.route('/book/<int:id>', methods=['PUT'])
def updateBook(id):
    book = Book.query.get(id)

    updatedBook = request.get_json()

    if (book is None) or (updatedBook is None): return jsonify({'Entity':'empty'}),404

    try:
        book.title = updatedBook.get('title')
        book.isbn = updatedBook.get('isbn')
        book.authorId = updatedBook.get('authorId')
        book.genreId = updatedBook.get('genreId')
        book.editorialId = updatedBook.get('editorialId')

        db.session.add(book)
        db.session.commit()

        return jsonify({'id':book.id,'update':'Book updated OK'})

    except Exception as e:
        return jsonify({'ERROR':str(e),'descr':updatedBook}),404

@paths.route('/book/<int:id>', methods=['DELETE'])
def deleteBook(id):
    book = Book.query.get(id)

    if book is None: return jsonify({'Entity':'empty'}),404

    try:
        db.session.delete(book)
        db.session.commit()

        return jsonify({'title':book.title,'delete':'Book deleted ok'})

    except Exception as e:
        return jsonify({'ERROR':str(e)}),404

@paths.route('/partner', methods=['POST'])
def createPartner():
    try:
        value = request.get_json()
        newPartner = Partner(value)
        db.session.add(newPartner)
        db.session.commit()

        return jsonify({
            "id":newPartner.id,
            "nickname":newPartner.nickname,
            "creation":"OK"
        }),201

    except Exception as e:
        return jsonify({'ERROR':str(e)}),404

@paths.route('/partner/<int:id>', methods=['GET'])
def getPartner(id):
    partner = Partner.query.get(id)

    if partner is None: return jsonify({'Entity':'empty'}),404

    return jsonify({
        'id':partner.id,
        'nickname':partner.nickname,
        'mail':partner.mail
    })

@paths.route('/partners', methods=['GET'])
def getPartners():
    partners = Partner.query.order_by(Partner.id).all()

    if not partners : return jsonify({'Entities':'empty'}),404

    return jsonify([{
        'id':partner.id,
        'nickname':partner.nickname,
        'mail':partner.mail
    }for partner in partners])

@paths.route('/partner/<int:id>', methods=['PUT'])
def updatePartner(id):
    partner = Partner.query.get(id)
    updatedPartner = request.get_json()

    if partner is None: return jsonify({'Entity':'empty'}),404

    # El id no se puede modificar para mantener coherencia 
    # con la creacion de nuevos socios
    try:
        partner.nickname = updatedPartner.get('nickname')
        partner.mail = updatedPartner.get('mail')

        db.session.add(partner)
        db.session.commit()

        return jsonify({'id':partner.id,'update':'Partner updated OK'})

    except Exception as e:
        return jsonify({'ERROR':str(e)}),404

@paths.route('/partner/<int:id>', methods=['DELETE'])
def deletePartner(id):
    partner = Partner.query.get(id)

    if partner is None: return jsonify({'Entity':'empty'}),404

    try:
        db.session.delete(partner)
        db.session.commit()

        return jsonify({'nickname':partner.nickname,'delete':'Partner deleted ok'})

    except Exception as e:
        return jsonify({'ERROR':str(e)}),404