#from serverDB import db
#from tests.configTesting import db
from app import db
class Author(db.Model):
    __tablename__ = 'author'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    surname = db.Column(db.String(100))

    def __init__(self, author):
        self.name = author.get('name')
        self.surname = author.get('surname')

class Genre(db.Model):
    __tablename__ = 'genre'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=True)

    def __init__(self, genre):
        self.name = genre.get('name')

class Editorial(db.Model):
    __tablename__ = 'editorial'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __init__(self, editorial):
        self.name = editorial.get('name')

class Book(db.Model):
    __tablename__ = 'book'
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(200))
    isbn = db.Column(db.String(50), unique=True)
    authorId = db.Column(db.Integer, db.ForeignKey('author.id'))
    genreId = db.Column(db.Integer, db.ForeignKey('genre.id'))
    editorialId = db.Column(db.Integer, db.ForeignKey('editorial.id'))

    def __init__(self, book):
        self.title = book.get('title')
        self.isbn = book.get('isbn')
        self.authorId = book.get('authorId')
        self.genreId = book.get('genreId')
        self.editorialId = book.get('editorialId')

class Partner(db.Model):
    __tablename__= 'partner'
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(100), unique=True)
    mail = db.Column(db.String(100), unique=True)

    def __init__(self, partner):
        self.nickname = partner.get('nickname')
        self.mail = partner.get('mail')
